/**
 * @file apps/main.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Application Tasks' Entrypoint
 * @version 0.1
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <apps/main.h>
#include <apps/led.h>

void startApplicationTasks(void) {
    printf("booting apps...\r\n");
    startLEDTask();
}
