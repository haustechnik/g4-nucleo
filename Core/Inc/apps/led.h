/**
 * @file apps/led.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief LED Blinking App Configuration and Entrypoint
 * @version 0.1
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

#define LED_TASK_STACK_SIZE 128

void startLEDTask(void);
