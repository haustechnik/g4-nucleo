/**
 * @file apps/main.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Common Application Configuration and Entrypoint
 * @version 0.1
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

#include <main.h>

#include <FreeRTOS.h>
#include <task.h>

#define NUNAVUT_ASSERT assert_param

void startApplicationTasks(void);
