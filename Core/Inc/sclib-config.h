/**
 * @file sclib-config.h
 * @author Lasse Fröhner (lasse@starcopter.com)
 * @date 2021-04-03
 *
 * @copyright Copyright (c) 2021 starcopter
 *
 */

#pragma once

// #define SCLIB__STDIN_UART_HANDLE (huart2)
#define SCLIB__STDOUT_UART_HANDLE (huart2)
// #define SCLIB__STDERR_UART_HANDLE (SCLIB__STDOUT_UART_HANDLE)

#define SCLIB__UART_HAL_IT
